package middleware

import (
	"fmt"
	"net/http"
	"time"

	"golang-crud-mysql-pooling/apps"
	"golang-crud-mysql-pooling/common"
	"golang-crud-mysql-pooling/model"

	"github.com/dgrijalva/jwt-go"
)

var expMins time.Duration = 2628000
var myKey = []byte("")
var SparkAuth string = ""

// IsAuthorized is the func for validating the JWT token
func IsAuthorized(endpoint func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header["Authorization"] != nil {
			token, err := jwt.Parse(r.Header["Authorization"][0], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("there was an error")
				}

				return myKey, nil
			})

			if err != nil {
				common.JSONResponse(w, model.AuthNotFound, err.Error(), http.StatusUnauthorized)
				return
			}

			r.Header.Add("X-Hash-Key", token.Claims.(jwt.MapClaims)["hash_key"].(string))

			if token.Valid {
				endpoint(w, r)
			}

		} else {
			common.JSONResponse(w, model.AuthNotFound, "", http.StatusUnauthorized)
			return
		}
	})
}

// IsStaticAuth is the func for validating static token
func IsStaticAuth(endpoint func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header["Authorization"] != nil {
			if r.Header["Authorization"][0] == apps.EnvConfigs.StaticToken {
				endpoint(w, r)
			} else {
				common.JSONResponse(w, model.AuthNotFound, "", http.StatusUnauthorized)
				return
			}
		} else {
			common.JSONResponse(w, model.AuthNotFound, "", http.StatusUnauthorized)
			return
		}
	})
}

// GenerateJWT is func to generate the token
func GenerateJWT(hashKey string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	//claims["user"] = "Erlangga"
	//@todo need to define the expired time
	claims["exp"] = time.Now().Add(time.Minute * expMins).Unix()
	claims["hash_key"] = hashKey

	tokenString, err := token.SignedString(myKey)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	return tokenString, nil
}

func BasicAuthMiddleware(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		//fmt.Println("username: ", user)
		//fmt.Println("password: ", pass)
		if !ok || !checkUsernameAndPassword(user, pass) {
			w.Header().Set("WWW-Authenticate", `Basic realm="Please enter your username and password for this site"`)
			w.WriteHeader(401)
			w.Write([]byte("Unauthorised.\n"))
			return
		}
		handler(w, r)
	}
}

func checkUsernameAndPassword(username, password string) bool {
	if SparkAuth == "" {
		SparkAuth = apps.EnvConfigs.SparkAuth

	}

	return (username + " " + password) == SparkAuth
	//return username == "dev.wgroup@gmail.com" && password == "wGroup_1tdev"
}