dev-build:
	rm -rf vendor/;git pull origin main;go mod tidy;go mod download;
	go mod vendor;go mod verify;rm mysql_pooling.tar;docker build -t mysql_pooling:latest .;
	docker save mysql_pooling:latest > mysql_pooling.tar;microk8s ctr image import mysql_pooling.tar
prod-build:
	rm -rf vendor/;git pull origin main;go mod tidy;go mod download;
	go mod vendor;go mod verify;rm mysql_pooling.tar;docker build -t mysql_pooling:latest .;
	docker save mysql_pooling:latest > mysql_pooling.tar;microk8s ctr image import mysql_pooling.tar