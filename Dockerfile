####### golang alpine 1.13.5 ########
# FROM golang@sha256:0991060a1447cf648bab7f6bb60335d1243930e38420bee8fec3db1267b84cfa as builder
FROM golang:1.19.3-alpine as builder

ENV GO111MODULE=on
ENV GOFLAGS=-mod=vendor

RUN apk add --no-cache tzdata
ENV TZ=Asia/Jakarta

WORKDIR '/app'

COPY go.mod .
COPY go.sum .

COPY . .

RUN go mod download
RUN go mod vendor
RUN go mod verify


# RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
RUN go build -o golang_mysql_pool
EXPOSE 8009

CMD ["./golang_mysql_pool"]