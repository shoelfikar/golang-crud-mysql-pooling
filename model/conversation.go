package model

type Incoming struct {
	Contact      Contact      `json:"contact"`
	Conversation Conversation `json:"conversation"`
	Message      IncMessage   `json:"message"`
	Type         string       `json:"type"`
}

type IncMessage struct {
	ID        string  `json:"id,omitempty"`
	ChannelID string  `json:"channelId,omitempty"`
	Content   Content `json:"content,omitempty"`
	Direction string  `json:"direction,omitempty"`
	Status    string  `json:"status,omitempty"`
	From      string  `json:"from,omitempty"`
	To        string  `json:"to,omitempty"`
	Platform  string  `json:"platform,omitempty"`
}

type Conversation struct {
	ID     string `json:"id"`
	Status string `json:"status,omitempty"`
}

type Contact struct {
	ID            string `json:"id"`
	MSISDN        string `json:"msisdn,omitempty"`
	ContactStatus string `json:"status,omitempty"`
}

type Content struct {
	Hsm        *Hsm         `json:"hsm,omitempty"`
	Text       *string      `json:"text,omitempty"`
	Audio      *OutAudio    `json:"audio,omitempty"`
	Image      *OutImage    `json:"image,omitempty"`
	Document   *OutDocument `json:"document,omitempty"`
	Video      *OutVideo    `json:"video,omitempty"`
	Sticker    *OutSticker  `json:"sticker,omitempty"`
	TextEmail  *string      `json:"text_email,omitempty"`
	Name       string       `json:"name,omitempty"`
	Components []Component  `json:"components,omitempty"`
}

type Hsm struct {
	Namespace    string   `json:"namespace"`
	TemplateName string   `json:"templateName,omitempty"`
	ElementName  string   `json:"element_name,omitempty"`
	Lang         Language `json:"language"`
	Localizable  []Params `json:"localizable_params,omitempty"`
	Params       []Params `json:"params,omitempty"`
}

type Language struct {
	Policy string `json:"policy"`
	Code   string `json:"code"`
}

type Params struct {
	Default string `json:"default,omitempty"`
}

type OutAudio struct {
	Caption string `json:"caption,omitempty"`
	Link    string `json:"url,omitempty"`
}

type OutImage struct {
	Caption string `json:"caption,omitempty"`
	Link    string `json:"url,omitempty"`
}

type OutDocument struct {
	Caption string `json:"caption,omitempty"`
	Link    string `json:"url,omitempty"`
}

type OutVideo struct {
	Caption string `json:"caption,omitempty"`
	Link    string `json:"url,omitempty"`
}

type OutSticker struct {
	Caption string `json:"caption,omitempty"`
	Link    string `json:"url,omitempty"`
}

type Component struct {
	Type       string    `json:"type"` //Type for header or body
	Parameters []PrmType `json:"parameters"`
}

type PrmType struct {
	Type     string        `json:"type"` //Type for document, image, text, currency, date_time
	Text     string        `json:"text,omitempty"`
	Document MediaDoc      `json:"document,omitempty"`
	Image    MediaImage    `json:"image,omitempty"`
	Currency MediaCurrency `json:"currency,omitempty"`
	DateTime MediaDate     `json:"date_time,omitempty"`
}

type MediaDate struct {
	Fallback   string `json:"fallback_value"`
	DayOfWeek  string `json:"day_of_week"`
	DayOfMonth string `json:"day_of_month"`
	Year       int    `json:"year"`
	Month      int    `json:"month"`
	Hour       int    `json:"hour"`
	Minute     int    `json:"minute"`
	Timestamp  int64  `json:"timestamp"`
}

type MediaCurrency struct {
	Fallback string `json:"fallback_value"`
	Code     string `json:"code"`
	Amount   string `json:"amount_1000"`
}

type MediaImage struct {
	Link string `json:"link"`
	Name string `json:"name"`
	Url  string `json:"url,omitempty"`
}

type MediaDoc struct {
	Link     string   `json:"link"`
	Provider Provider `json:"provider"`
	Filename string   `json:"filename"`
}

type Provider struct {
	Name string `json:"name"`
}
