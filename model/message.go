package model

//ErrorMessage struct for general error
type ErrorMessage struct {
	Message        string `json:"message,omitempty"`
	Data           string `json:"data,omitempty"`
	SysMessage     string `json:"system_message,omitempty"`
	Code           int    `json:"code,omitempty"`
	RespMessage    string `json:"response_from_vendor,omitempty"`
	ReqMessage     string `json:"request_to_vendor,omitempty"`
	ReqSesame      string `json:"request_from_sesame,omitempty"`
	MessageID      string `json:"id,omitempty"`
	ConversationID string `json:"conversationId,omitempty"`
	ChannelID      string `json:"channelId,omitempty"`
}

type HttpResponse struct {
	Code    int         `json:"code,omitempty"`
	Message string      `json:"message,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

type HttpRequest struct {
	RequestBody string `json:"request_body,"`
}

type RequestInfo struct {
	Host             string   `json:"host,omitempty"`
	RequestUri       string   `json:"request_uri,omitempty"`
	RemoteAddress    string   `json:"remote_address,omitempty"`
	TransferEncoding []string `json:"transfer_encoding,omitempty"`
}
