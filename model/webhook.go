package model

type Webhook struct {
	Id          int    `json:"id,omitempty"`
	RequestBody string `json:"request_body,omitempty"`
	RequestInfo string `json:"user_info,omitempty"`
	CreatedAt   string `json:"created_at,omitepmty"`
	CreatedBy   string `json:"created_by,omitpemty"`
}