package apps

import (
	"database/sql"
	"log"
	"time"
)

func NewDB() *sql.DB {
	db, err := sql.Open("mysql", EnvConfigs.DbUrl)

	if err != nil {
		log.Println(err.Error())
		return nil
	}

	db.SetMaxIdleConns(50)
	db.SetMaxOpenConns(150)
	db.SetConnMaxLifetime(120 * time.Minute)
	db.SetConnMaxIdleTime(5 * time.Minute)

	return db
}