package router

import (
	"fmt"
	"golang-crud-mysql-pooling/middleware"
	"net/http"

	"github.com/gorilla/mux"
)

type muxRouter struct{}

var (
	muxDispatcher = mux.NewRouter()
)

func NewMuxRouter() Router {
	return &muxRouter{}
}

func (*muxRouter) GET(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	muxDispatcher.Handle(uri, middleware.IsAuthorized(f)).Methods("GET")
	//muxDispatcher.HandleFunc(uri, f).Methods("GET")
}
func (*muxRouter) POST(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	// muxDispatcher.Handle(uri, api.IsAuthorized(f)).Methods("POST")
	muxDispatcher.HandleFunc(uri, f).Methods("POST")
}
func (*muxRouter) SERVE(port string) {
	//fmt.Println(http.ListenAndServe(port, muxDispatcher))
	// fmt.Println(http.ListenAndServeTLS(port, "server.crt", "server.key", muxDispatcher))
	server := &http.Server{
		Addr:         port,
		Handler:      muxDispatcher,
		// ReadTimeout:  10 * time.Second, // Set the read timeout to 10 seconds
		// WriteTimeout: 10 * time.Second, // Set the write timeout to 10 seconds
	}
	// fmt.Println(http.ListenAndServe(port, muxDispatcher))
	// fmt.Println(server.ListenAndServe())
	fmt.Println(server.ListenAndServeTLS("server.crt", "server.key"))
}

func (*muxRouter) POSTLogin(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	muxDispatcher.HandleFunc(uri, f).Methods("POST")
}

func (*muxRouter) POSTStatic(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	muxDispatcher.Handle(uri, middleware.IsStaticAuth(f)).Methods("POST")
	//muxDispatcher.HandleFunc(uri, f).Methods("POST")
}

func (*muxRouter) POSTBasicAuth(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	muxDispatcher.Handle(uri, middleware.BasicAuthMiddleware(f)).Methods("POST")
	//muxDispatcher.HandleFunc(uri, f).Methods("POST")
}

func (*muxRouter) POSTAuth(uri string, f func(w http.ResponseWriter, r *http.Request)) {
	muxDispatcher.Handle(uri, middleware.IsAuthorized(f)).Methods("POST")
}
