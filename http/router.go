package router

import (
	"net/http"
)

type Router interface {
	GET(uri string, f func(w http.ResponseWriter, r *http.Request))
	POST(uri string, f func(w http.ResponseWriter, r *http.Request))
	POSTLogin(uri string, f func(w http.ResponseWriter, r *http.Request))
	POSTStatic(uri string, f func(w http.ResponseWriter, r *http.Request))
	POSTBasicAuth(uri string, f func(w http.ResponseWriter, r *http.Request))
	POSTAuth(uri string, f func(w http.ResponseWriter, r *http.Request))
	SERVE(port string)
}
