package common

import (
	"encoding/json"
	"net/http"
	"golang-crud-mysql-pooling/model"
)

func JSONResp(w http.ResponseWriter, errStr *model.ErrorMessage) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(errStr.Code)
	json.NewEncoder(w).Encode(errStr)
}

func JSONResponse(w http.ResponseWriter, message string, sysMessage string, code int) {
	var errstr model.ErrorMessage
	errstr.Message = message
	errstr.SysMessage = sysMessage
	errstr.Code = code
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(errstr)
}