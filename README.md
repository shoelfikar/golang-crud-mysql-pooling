# enigma
# route minikube to read local image
`eval $(minikube docker-env)`

# build image for docker
`docker build -t mysql_pooling:latest .`

# run the container using image
`docker run -p 8000:8000 mysql_pooling:latest`

# in your local machine, `export GOSUMDB=off` if go mod tidy having terminal prompts disabled error
```
export GOFLAGS=-mod=vendor
export GO111MODULE=on
export GOSUMDB=off
go mod init
go mod tidy
go mod download
go mod vendor
go mod verify
```

# .env file
`JWT_KEY=thisisenigmakey`
# for mysql host access from docker
`DB_URL=admin:admin@(host.docker.internal:3306)/webhook`
# for mysql in local machine
```
DB_URL_LOCAL=admin:admin@(127.0.0.1:3306)/webhook
```
