package repositories

import (
	"context"
	"database/sql"
	"golang-crud-mysql-pooling/interfaces"
	"golang-crud-mysql-pooling/model"
	"log"
)

type webhookRepo struct{}

func NewWebhookRepository() interfaces.WebhookInterface {
	return &webhookRepo{}
}

func (*webhookRepo) WebhookRequest(wh *model.Webhook, ctx context.Context, tx *sql.Tx) (*model.Webhook, error) {
	
	SQL := "INSERT INTO webhook_logs(request_body, request_info, created_by) VALUES(?,?,?)"

	result, err := tx.ExecContext(ctx, SQL, wh.RequestBody, wh.RequestInfo, wh.CreatedBy)

	if err != nil {
		log.Println(err.Error())
		return &model.Webhook{}, err
	}

	insertId, _ := result.LastInsertId()

	lastId := int(insertId)

	wh.Id = lastId

	return wh, nil
}