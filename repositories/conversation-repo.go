package repositories

import (
	"context"
	"database/sql"
	"encoding/json"
	"golang-crud-mysql-pooling/interfaces"
	"golang-crud-mysql-pooling/model"
	"log"
	"net/http"
	"strings"
)

type conversationRepo struct{}

func NewConversationRepo() interfaces.ConversationRepository {
	return &conversationRepo{}
}

func (*conversationRepo) ReceiveConversation(conv model.Incoming, ctx context.Context, tx *sql.Tx) *model.ErrorMessage {
	var errors model.ErrorMessage
	var msgJson []byte
	var contactJson []byte
	var cnvJson []byte
	var reqJson []byte
	var channel string = ""

	clientID := getClientID(conv.Message.ChannelID, ctx , tx)


	sqlQuery := "INSERT INTO conversation_logs (client_id, contact_id, message_id, conversation_id, conversation_status, channel_id, platform, channel, direction, message_status, `from`, `to`, msisdn, contact_status, `type`, request_body, message_json, contact_json, conversation_json) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

	reqJson, _ = json.Marshal(conv)
	msgJson, _ = json.Marshal(conv.Message)
	contactJson, _ = json.Marshal(conv.Contact)
	cnvJson, _ = json.Marshal(conv.Conversation)

	plat := strings.ToLower(conv.Message.Platform)
	switch plat {
	case "whatsapp_sandbox":
		channel = "Whatsapp"
	case "whatsapp":
		//do for whatsapp on Production
		channel = "Whatsapp"
	case "facebook":
		channel = "Facebook"
	case "telegram":
		channel = "Telegram"
	}

	res, err := tx.ExecContext(ctx, sqlQuery, &clientID, &conv.Contact.ID, &conv.Message.ID, &conv.Conversation.ID, &conv.Conversation.Status, &conv.Message.ChannelID, &conv.Message.Platform, channel, &conv.Message.Direction, &conv.Message.Status, &conv.Message.From, &conv.Message.To, &conv.Contact.MSISDN, &conv.Contact.ContactStatus, &conv.Type, reqJson, msgJson, contactJson, cnvJson)
	lastID, _ := res.LastInsertId()

	if err != nil {
		log.Println(err.Error())
		tx.Rollback()
		errors.Message = model.QueryErr
		errors.Data = string(reqJson)
		errors.SysMessage = err.Error()
		errors.Code = http.StatusInternalServerError
		return &errors
	}
	if err != nil {
		tx.Rollback()
		errors.Message = model.LastIDErr
		errors.Data = string(reqJson)
		errors.SysMessage = err.Error()
		errors.Code = http.StatusInternalServerError
		log.Println(lastID)
		return &errors
	}


	switch conv.Message.Status {
	case "received":
		tx.Commit()
	case "pending":
		statusQuery := "INSERT INTO status_pending (contact_id, message_id, conversation_id, conversation_status, channel_id, platform, channel, direction, message_status, `from`, `to`, msisdn, contact_status, `type`, request_body, message_json, contact_json, conversation_json) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

		resStatus, err2 := tx.ExecContext(ctx, statusQuery, &conv.Contact.ID, &conv.Message.ID, &conv.Conversation.ID, &conv.Conversation.Status, &conv.Message.ChannelID, &conv.Message.Platform, channel, &conv.Message.Direction, &conv.Message.Status, &conv.Message.From, &conv.Message.To, &conv.Contact.MSISDN, &conv.Contact.ContactStatus, &conv.Type, reqJson, msgJson, contactJson, cnvJson)

		if err2 != nil {
			log.Println(err2.Error())
			tx.Rollback()
			errors.Message = model.QueryErr
			errors.Data = string(reqJson)
			errors.SysMessage = err2.Error()
			errors.Code = http.StatusInternalServerError
			return &errors
		}

		lastID, err3 := resStatus.LastInsertId()
		if err3 != nil {
			tx.Rollback()
			errors.Message = model.LastIDErr
			errors.Data = string(reqJson)
			errors.SysMessage = err3.Error()
			errors.Code = http.StatusInternalServerError
			log.Println(lastID)
			return &errors
		}

		tx.Commit()
	case "sent":
		statusQuery := "INSERT INTO status_sent (contact_id, message_id, conversation_id, conversation_status, channel_id, platform, channel, direction, message_status, `from`, `to`, msisdn, contact_status, `type`, request_body, message_json, contact_json, conversation_json) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

		resStatus, err2 := tx.ExecContext(ctx, statusQuery, &conv.Contact.ID, &conv.Message.ID, &conv.Conversation.ID, &conv.Conversation.Status, &conv.Message.ChannelID, &conv.Message.Platform, channel, &conv.Message.Direction, &conv.Message.Status, &conv.Message.From, &conv.Message.To, &conv.Contact.MSISDN, &conv.Contact.ContactStatus, &conv.Type, reqJson, msgJson, contactJson, cnvJson)

		if err2 != nil {
			log.Println(err2.Error())
			tx.Rollback()
			errors.Message = model.QueryErr
			errors.Data = string(reqJson)
			errors.SysMessage = err2.Error()
			errors.Code = http.StatusInternalServerError
			return &errors
		}

		lastID, err3 := resStatus.LastInsertId()
		if err3 != nil {
			tx.Rollback()
			errors.Message = model.LastIDErr
			errors.Data = string(reqJson)
			errors.SysMessage = err3.Error()
			errors.Code = http.StatusInternalServerError
			log.Println(lastID)
			return &errors
		}

		tx.Commit()
	case "delivered":
		statusQuery := "INSERT INTO status_delivered (contact_id, message_id, conversation_id, conversation_status, channel_id, platform, channel, direction, message_status, `from`, `to`, msisdn, contact_status, `type`, request_body, message_json, contact_json, conversation_json) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

		resStatus, err2 := tx.ExecContext(ctx, statusQuery, &conv.Contact.ID, &conv.Message.ID, &conv.Conversation.ID, &conv.Conversation.Status, &conv.Message.ChannelID, &conv.Message.Platform, channel, &conv.Message.Direction, &conv.Message.Status, &conv.Message.From, &conv.Message.To, &conv.Contact.MSISDN, &conv.Contact.ContactStatus, &conv.Type, reqJson, msgJson, contactJson, cnvJson)

		if err2 != nil {
			log.Println(err2.Error())
			tx.Rollback()
			errors.Message = model.QueryErr
			errors.Data = string(reqJson)
			errors.SysMessage = err2.Error()
			errors.Code = http.StatusInternalServerError
			return &errors
		}

		lastID, err3 := resStatus.LastInsertId()
		if err3 != nil {
			tx.Rollback()
			errors.Message = model.LastIDErr
			errors.Data = string(reqJson)
			errors.SysMessage = err3.Error()
			errors.Code = http.StatusInternalServerError
			log.Println(lastID)
			return &errors
		}

		tx.Commit()
	case "read":
		statusQuery := "INSERT INTO status_read (contact_id, message_id, conversation_id, conversation_status, channel_id, platform, channel, direction, message_status, `from`, `to`, msisdn, contact_status, `type`, request_body, message_json, contact_json, conversation_json) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

		resStatus, err2 := tx.ExecContext(ctx, statusQuery, &conv.Contact.ID, &conv.Message.ID, &conv.Conversation.ID, &conv.Conversation.Status, &conv.Message.ChannelID, &conv.Message.Platform, channel, &conv.Message.Direction, &conv.Message.Status, &conv.Message.From, &conv.Message.To, &conv.Contact.MSISDN, &conv.Contact.ContactStatus, &conv.Type, reqJson, msgJson, contactJson, cnvJson)

		if err2 != nil {
			log.Println(err2.Error())
			tx.Rollback()
			errors.Message = model.QueryErr
			errors.Data = string(reqJson)
			errors.SysMessage = err2.Error()
			errors.Code = http.StatusInternalServerError
			return &errors
		}

		lastID, err3 := resStatus.LastInsertId()
		if err3 != nil {
			tx.Rollback()
			errors.Message = model.LastIDErr
			errors.Data = string(reqJson)
			errors.SysMessage = err3.Error()
			errors.Code = http.StatusInternalServerError
			log.Println(lastID)
			return &errors
		}

		tx.Commit()
	case "failed":
		statusQuery := "INSERT INTO status_failed (contact_id, message_id, conversation_id, conversation_status, channel_id, platform, channel, direction, message_status, `from`, `to`, msisdn, contact_status, `type`, request_body, message_json, contact_json, conversation_json) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

		resStatus, err2 := tx.ExecContext(ctx, statusQuery, &conv.Contact.ID, &conv.Message.ID, &conv.Conversation.ID, &conv.Conversation.Status, &conv.Message.ChannelID, &conv.Message.Platform, channel, &conv.Message.Direction, &conv.Message.Status, &conv.Message.From, &conv.Message.To, &conv.Contact.MSISDN, &conv.Contact.ContactStatus, &conv.Type, reqJson, msgJson, contactJson, cnvJson)

		if err2 != nil {
			log.Println(err2.Error())
			tx.Rollback()
			errors.Message = model.QueryErr
			errors.Data = string(reqJson)
			errors.SysMessage = err2.Error()
			errors.Code = http.StatusInternalServerError
			return &errors
		}

		lastID, err3 := resStatus.LastInsertId()
		if err3 != nil {
			tx.Rollback()
			errors.Message = model.LastIDErr
			errors.Data = string(reqJson)
			errors.SysMessage = err3.Error()
			errors.Code = http.StatusInternalServerError
			log.Println(lastID)
			return &errors
		}
		tx.Commit()
	default:
		tx.Commit()
	}

	sCaseChannel := strings.ToLower(channel)
	if sCaseChannel == "whatsapp_sandbox" {
		sCaseChannel = "whatsapp"
	}



	errors.Message = model.Success
	errors.Data = string(reqJson)
	errors.Code = http.StatusOK

	return &errors
}


func getClientID(channelId string, ctx context.Context, tx *sql.Tx) int {
	var clientID int


	cntQry := "select distinct client_id from enigma.client_vendors cv where channel_id = ?"
	err := tx.QueryRowContext(ctx, cntQry, channelId).Scan(&clientID)

	if err != nil {
		log.Println(err.Error())
		return 0
	}

	return clientID
}