package main

import (
	"golang-crud-mysql-pooling/apps"
	"golang-crud-mysql-pooling/controller"
	router "golang-crud-mysql-pooling/http"
	"golang-crud-mysql-pooling/repositories"
	"golang-crud-mysql-pooling/services"
	"log"
	"os"

	// "os"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	apps.InitEnvConfigs()

	db := apps.NewDB()

	serverPort := apps.EnvConfigs.LocalServerPort

	if serverPort == "" {
		serverPort = os.Getenv("APPPORT")
	}
	

	router := router.NewMuxRouter()

	webhookRepository := repositories.NewWebhookRepository()
	webhookService := services.NewWebhookService(webhookRepository, db)
	webhookController := controller.NewWebhookController(webhookService)

	conversationRepository := repositories.NewConversationRepo()
	conversationService := services.NewConversationService(conversationRepository, db)
	conversationController := controller.NewConversationController(conversationService)

	router.POST("/api/v1/webhook", webhookController.Webhook)
	router.POST("/api/v1/conversation", conversationController.ReceiveConversation)

	// log.SetOutput(logFile)

	log.Println("server running on port " + serverPort)

	router.SERVE(":" + serverPort)
}