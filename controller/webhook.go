package controller

import (
	"encoding/json"
	"golang-crud-mysql-pooling/model"
	"golang-crud-mysql-pooling/services"
	"log"
	"net/http"
)

type webhookController struct {
	WebhookService services.WebhookService
}

type WebhookController interface {
	Webhook(w http.ResponseWriter, r *http.Request)
}

func NewWebhookController(webhookService services.WebhookService) WebhookController {
	return &webhookController{
		WebhookService: webhookService,
	}
}

func (controller *webhookController) Webhook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var response model.HttpResponse

	data, err := controller.WebhookService.WebhookRequest(r)

	if err != nil {
		log.Println(err.Error())
		response.Code = 500
		response.Message = err.Error()
		w.WriteHeader(500)
		json.NewEncoder(w).Encode(response)
		return
	}

	response.Code = 200
	response.Data = data
	response.Message = "success"

	log.Println("success insert webhook log")

	w.WriteHeader(200)
	json.NewEncoder(w).Encode(response)
}