package controller

import (
	"encoding/json"
	"golang-crud-mysql-pooling/common"
	"golang-crud-mysql-pooling/model"
	"golang-crud-mysql-pooling/services"
	"log"
	"net/http"
)

type conversationController struct{
	ConversationService services.ConversationService
}

type ConversationController interface {
	ReceiveConversation(w http.ResponseWriter, r *http.Request)
}

func NewConversationController(service services.ConversationService) ConversationController {
	return &conversationController{
		ConversationService: service, 
	}
}

func (controller *conversationController) ReceiveConversation(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var conv model.Incoming


	_ = json.NewDecoder(r.Body).Decode(&conv)


	//send API to Vendor

	resp := controller.ConversationService.ReceiveConversation(conv)

	if resp.Code == 200 {
		log.Println("success insert data")
	}else{
		log.Println("failed insert data with error : " + resp.SysMessage)
	}

	common.JSONResp(w, resp)
}
