package services

import (
	"context"
	"database/sql"
	"encoding/json"
	"golang-crud-mysql-pooling/interfaces"
	"golang-crud-mysql-pooling/model"
	"net/http"
)

type webhookService struct{
	WebhookRepository interfaces.WebhookInterface
	DB 				  *sql.DB
}

type WebhookService interface {
	WebhookRequest(r *http.Request) (*model.Webhook, error)
}

func NewWebhookService(webhookRepository interfaces.WebhookInterface, DB *sql.DB) WebhookService {
	return &webhookService{
		WebhookRepository: webhookRepository,
		DB: DB,
	}
}

func (service *webhookService) WebhookRequest(r *http.Request) (*model.Webhook, error) {
	tx, err := service.DB.Begin()
	
	if err != nil {
		return &model.Webhook{}, err
	}

	defer tx.Commit()

	ctx := context.Background()

	var reqinfo model.RequestInfo
	var reqbody interface{}
	var wh model.Webhook

	_ = json.NewDecoder(r.Body).Decode(&reqbody)

	reqinfo.Host = r.Host
	// Get the IP address of the client that made the request
    ip := r.Header.Get("X-Forwarded-For")
    
    // If the header is empty, use the RemoteAddr field
    if ip == "" {
        ip = r.RemoteAddr
    }

	reqinfo.RemoteAddress = ip
	reqinfo.RequestUri = r.RequestURI
	reqinfo.TransferEncoding = r.TransferEncoding

	rb, _ := json.Marshal(reqbody)

	wh.RequestBody = string(rb)

	rinfo, _ := json.Marshal(reqinfo)

	wh.RequestInfo = string(rinfo)

	wh.CreatedBy = "system"



	whk, err2 := service.WebhookRepository.WebhookRequest(&wh, ctx, tx)

	return whk, err2
}