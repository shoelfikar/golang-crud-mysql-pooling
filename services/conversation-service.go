package services

import (
	"context"
	"database/sql"
	"golang-crud-mysql-pooling/interfaces"
	"golang-crud-mysql-pooling/model"
	"net/http"
)

type conversationService struct {
	ConversationRepository interfaces.ConversationRepository
	DB *sql.DB
}

type ConversationService interface {
	ReceiveConversation(conv model.Incoming) *model.ErrorMessage
}

func NewConversationService(convRepo interfaces.ConversationRepository, DB *sql.DB) ConversationService {
	return &conversationService{
		ConversationRepository: convRepo,
		DB: DB,
	}
}

func (conver *conversationService) ReceiveConversation(conv model.Incoming) *model.ErrorMessage {
	tx, err := conver.DB.Begin()
	
	if err != nil {
		return &model.ErrorMessage{
			Code: http.StatusInternalServerError,
			Message: "Internal Server Error",
			SysMessage: err.Error(),
		}
	}

	ctx := context.Background()

	resp := conver.ConversationRepository.ReceiveConversation(conv, ctx, tx)

	return resp
}