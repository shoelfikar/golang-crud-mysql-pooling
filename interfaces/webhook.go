package interfaces

import (
	"context"
	"database/sql"
	"golang-crud-mysql-pooling/model"
)

type WebhookInterface interface {
	WebhookRequest(wh *model.Webhook, ctx context.Context, tx *sql.Tx) (*model.Webhook, error)
}