package interfaces

import (
	"context"
	"database/sql"
	"golang-crud-mysql-pooling/model"
)

type ConversationRepository interface {
	ReceiveConversation(conv model.Incoming, ctx context.Context, tx *sql.Tx) *model.ErrorMessage
}